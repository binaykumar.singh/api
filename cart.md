## 6. Cart

In this exercise, you will add a product and a promotion to your cart. 

### Add product to cart

* Open the `Add product to cart` request under `carts -> cart_items` folder.
* Update the `id` variable to `{{productId}}`. > Note the lower case `d`.
* Update the quantity to 3.
* Click `Send`.
* Verify the response.

### Add promotion to cart

* Open the `Add a promotion` request under `carts -> cart_items` folder.
* Update the code to `SPECIAL`.
* Click `Send`.
* Verify the response.

[Next: 7. Customers](./customers.md)
