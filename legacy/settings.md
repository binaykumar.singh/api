## 3. Settings

In this exercise, you will retrieve a list of current settings and then update the calculation method. 

### Get settings

* Open the `Get all settings` request under `settings` folder.
* Click `Send`.
* You should see a full listing of settings values for the training store.

### Update calculation method

In order for the cart to properly calculate the promotion you're creating in the next exercise, you will need the calculation method set to `line`. 

* Open the `Update Settings` request under `Settings` folder.
* In the `Body` section, Update the `calculation_method` to `line`:

```json
{
	"data": {
	  "type": "settings",
	  "calculation_method": "line"
	}
}
```

* Click `Send`.
* Verify the response.

[Next: 4. Promotions](./promotions.md)
