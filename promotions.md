## 5. Promotions

In this exercise, you will create a promotion and associated promotion code. 

### Create X for Y promotion

* Open the `Create a X for Y discount promotion` request under `promotions -> promotions_management` folder.
* Update the `Body` section with the following fields: 
    * name: My X for Y Promotion Example
    * description: Description for my X for Y promotion example
    * schema:

    ```json
    {
      "x": 3,
      "y": 2,
      "targets": ["MPTP"]
    }
    ```

    * start: 2020-01-01
    * end: 2030-01-01

* Click `Send`.
> The script in the Tests section of the request, stores the returned ID in a variable called `promotionID`.

### Create promotion code

* Open the `Create promotion codes` request under `promotions -> promotion_codes` folder.
* Create a promotion code with the code as `SPECIAL`.
* Click `Send`.
* Verify the response.

[Next: 6. Cart](./cart.md)
