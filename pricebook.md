## 3. Price Book

In this exercise, you will create a price book with a price for your product. 

### Create a new price book

* Open the `Create a price book` request under `pricebooks` folder.
* Create a product with the following fields: 
    * name: My Training Price Book
    * description: My Training Price Book
* Click `Send`. The new price book id is saved to a `pricebookId` variable.
* Verify the response.

### Get a price book 

* Open the `Get a pricebook` request under `pricebooks` folder and click `Send`. 
* Verify the response matches the price book you created above. 

### Create a new product price

* Open the `Create a price` request under `pricebooks` folder. 
* Create a price with the following fields: 
    * currencies: USD; 250
* The `productSku` value is already filled in with a variable.
* Click `Send`. The new price id is saved to a `priceId` variable.
* Verify the response.

### Get a product price

* Open the `Get product price by id` request under `pricebooks` folder and click `Send`. 
* Verify the response matches the product price you created above.

[Next: 4. Catalog](./catalog.md)
